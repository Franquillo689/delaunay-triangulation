PointList P;
TriangleList triangulation;
PVector p1,p2,p3;
Triangle superTriangle;

int points = 10;

void setup(){
  //fullScreen();
  size(500,500);
  background(0);
  noLoop();
  
  TriangleList triangulation;
  triangulation = new TriangleList(points);
  
  p1 = new PVector(0,0);
  p2 = new PVector(width,0);
  p3 = new PVector(width/2,height*2);
  superTriangle = new Triangle(p1,p2,p3);
  
  P = new PointList(points,p1,p2,p3);
  
  triangulation.addTriangle(superTriangle);
  
  for(int i = 0;i < points;i++){
    TriangleList badTriangles;
    badTriangles = new TriangleList(points);
    for(int j = 0;j < triangulation.len;j++){
      if(triangulation.getTriangle(j).isInside(P.getPoint(i))){
        badTriangles.addTriangle(triangulation.getTriangle(j));
      }
    }
    SidesArray polygon;
    /*
    Given n triangles forming a
    polygon, the maximum sides
    the polygon can have is n+2
    */
    polygon = new SidesArray(badTriangles.len+2);
    polygon.makePolygon(badTriangles);
    for(int j = 0;j < badTriangles.len;j++){
      triangulation.removeTriangle(badTriangles.getTriangle(j));
    }
    
    for(int j = 0;j < polygon.len;j++){
      PVector a = polygon.getSide(j).getPointA();
      PVector b = polygon.getSide(j).getPointB();
      if(triangulation.isTriangle(a,b,P.getPoint(i))){
        Triangle tri = new Triangle(P.getPoint(i),a,b);
        triangulation.addTriangle(tri);
      }
    }
    
  }
  for(int i = 0;i < triangulation.len;i++){
    if(triangulation.getTriangle(i).isTrianglePoint(p1)||
       triangulation.getTriangle(i).isTrianglePoint(p2)||
       triangulation.getTriangle(i).isTrianglePoint(p3)){
       triangulation.removeTriangle(i);
       i--;
     }
  }
  triangulation.drawTriangles();
  for(int i = 0;i < P.len;i++){
    P.drawPoints();
  }
   //<>//
}

/*
function BowyerWatson (pointList)
    // pointList is a set of coordinates defining the points to be triangulated
    triangulation := empty triangle mesh data structure
    add super-triangle to triangulation // must be large enough to completely contain all the points in pointList
    for each point in pointList do // add all the points one at a time to the triangulation
        badTriangles := empty set
        for each triangle in triangulation do // first find all the triangles that are no longer valid due to the insertion
            if point is inside circumcircle of triangle
                add triangle to badTriangles
        polygon := empty set
        for each triangle in badTriangles do // find the boundary of the polygonal hole
            for each edge in triangle do
                if edge is not shared by any other triangles in badTriangles
                    add edge to polygon
        for each triangle in badTriangles do // remove them from the data structure
            remove triangle from triangulation
        for each edge in polygon do // re-triangulate the polygonal hole
            newTri := form a triangle from edge to point
            add newTri to triangulation
    for each triangle in triangulation // done inserting points, now clean up
        if triangle contains a vertex from original super-triangle
            remove triangle from triangulation
    return triangulation
*/
