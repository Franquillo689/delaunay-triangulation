class SidesArray {
  PolygonSide[] PS;
  int max,len;
  SidesArray(int m){
    max = m;
    len = 0;
    PS = new PolygonSide[max];
  }
  
  PolygonSide getSide(int index){
    return PS[index];
  }
  
  void removeSide(int index){
    PS[index] = PS[len-1];
    PS[len-1] = null;
    len--;
  }
  
  boolean addSide(PolygonSide side){
    boolean added = false;
    if(len < max){
      PS[len] = side;
      len++;
      added = true;
    } 
    return added;
  }
  
  void addSides(Triangle t){
    boolean[] sideIsIn;
    sideIsIn = new boolean[3];
    sideIsIn[0] = true;
    sideIsIn[1] = true;
    sideIsIn[2] = true;
    PolygonSide[] triangleSides;
    triangleSides = new PolygonSide[3];
    triangleSides[0] = new PolygonSide(t.a,t.b);
    triangleSides[1] = new PolygonSide(t.a,t.c);
    triangleSides[2] = new PolygonSide(t.c,t.b);
    for(int i = 0;i < len&&(sideIsIn[0]||sideIsIn[1]||sideIsIn[2]);i++){
      for(int j = 0;j < 3;j++){
        if(PS[i] != null && PS[i].equals(triangleSides[j])){
          sideIsIn[j] = false;
          removeSide(i);
        }
      }
    }
    for(int i = 0;i < 3;i++){
      if(sideIsIn[i]){
        addSide(triangleSides[i]);
      }
    }
  }
  
  void makePolygon(TriangleList T){
    for(int i = 0;i < T.len;i++){
      addSides(T.getTriangle(i));
    }
  }
  
}
