class PointList {
  PVector[] P;
  int len;
  PointList(int n, PVector p1, PVector p2, PVector p3){
    P = new PVector[n];
    len = n;
    P[0] = p1;
    P[1] = p2;
    P[2] = p3;
    for(int i = 3;i < n;i++){
      P[i] = new PVector(random(10,width-10),random(10,height-10));
    }
  }
  
  PVector getPoint(int index){
    return P[index];
  }
  
  void drawPoints(){
    noStroke();
    fill(255,0,0);
    for(int i = 0;i < len;i++){
      circle(P[i].x,P[i].y,5);
    }
  }
}
