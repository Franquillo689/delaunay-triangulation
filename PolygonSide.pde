class PolygonSide {
  PVector[] side;
  PolygonSide(PVector a, PVector b){
    side = new PVector[2];
    side[0] = a;
    side[1] = b;
  }
  
  PVector getPointA(){
    return side[0];
  }
  PVector getPointB(){
    return side[1];  
  }  
  boolean equals(PolygonSide PS){
    return (side[0] == PS.side[0] && side[1] == PS.side[1]) || (side[1] == PS.side[0] && side[0] == PS.side[1]);
  }
}
