class Triangle {
  PVector a,b,c,d;
  public Triangle(PVector p1, PVector p2, PVector p3){
    a = p1;
    b = p2;
    c = p3;
    float x,y;
    y = 0.5*(p1.mag()*p1.mag()*(p2.x-p3.x)-p2.mag()*p2.mag()*(p1.x-p3.x)+p3.mag()*p3.mag()*(p1.x-p2.x))/((p3.y-p1.y)*(p1.x-p2.x)-(p2.y-p1.y)*(p1.x-p3.x));
    x = (0.5*(p1.mag()*p1.mag()-(p2.mag()*p2.mag()))+y*(p2.y-p1.y))/(p1.x-p2.x);
    d = new PVector(x,y);
  }
  void drawSides(){
    triangle(a.x,a.y,b.x,b.y,c.x,c.y);
  }
  void drawEdges(){
    fill(255,0,0);
    noStroke();
    circle(a.x,a.y,5);
    circle(b.x,b.y,5);
    circle(c.x,c.y,5);
  }
  
  boolean isInside(PVector p){
    return !(d.dist(p) > d.dist(a));
  }
  
  boolean isTrianglePoint(PVector z){
    return z == a || z == b || z == c;
  }
  
}
