class TriangleList {
  int len,max;
  Triangle[] T;
  
  TriangleList(int points){
    max = points*(points-1)*(points-2)/6; // See https://www.quora.com/How-many-triangles-can-be-drawn-from-N-given-points-on-a-circle
    T = new Triangle[max];
    len = 0;
  }
  
  Triangle getTriangle(int index){
    return T[index];
  }
  
  boolean moveTriangleTo(int index, TriangleList triList){
    boolean moved = false;
    if(index < len && triList.addTriangle(T[index])){
      T[index] = T[len-1];
      T[len-1] = null;
      len--;
      moved = true;
    }
    return moved;
  }
  
  boolean addTriangle(Triangle tri){
    boolean added = false;
    if(len < max){
      T[len] = tri;
      len++;
      added = true;
    } 
    return added;
  }
  
  boolean removeTriangle(Triangle tri){
    boolean removed = false;
    for(int i = 0;i < len&&!removed;i++){
      if(T[i] == tri){
        T[i] = T[len-1];
        T[len-1] = null;
        len--;
        removed = true;
      }
    }
    return removed;
  }
  
  boolean removeTriangle(int index){
    if(index < len){
      T[index] = T[len-1];
      T[len-1] = null;
      len--;
      return true;
    }
    return false;
  }
  
  boolean isTriangle(PVector a, PVector b, PVector c){
    // Returns true if the points a,b,c form a triangle.
    return a != null && b != null && c != null && (b.x-a.x)*(c.y-a.y) != (b.y-a.y)*(c.x-a.x);
  }
  
  void drawTriangles(){
    noFill();
    stroke(255);
    for(int i = 0;i < len;i++){
      T[i].drawSides();
    }
  }
  
}
